import { createAction } from '@reduxjs/toolkit';
import { StorageKey } from 'src/common/enums/enums';
import {
  storage as storageService,
  auth as authService
} from 'src/services/services';

const ActionType = {
  SET_USER: 'profile/set-user',
  SET_USERNAME_ERROR: 'profile/set-username-error'
};

const setUser = createAction(ActionType.SET_USER, user => ({
  payload: {
    user
  }
}));

const setUsernameError = createAction(ActionType.SET_USERNAME_ERROR, error => ({
  payload: {
    error
  }
}));

const login = request => async dispatch => {
  const { user, token } = await authService.login(request);

  storageService.setItem(StorageKey.TOKEN, token);
  dispatch(setUser(user));
};

const register = request => async dispatch => {
  const { user, token } = await authService.registration(request);

  storageService.setItem(StorageKey.TOKEN, token);
  dispatch(setUser(user));
};

const logout = () => dispatch => {
  storageService.removeItem(StorageKey.TOKEN);
  dispatch(setUser(null));
};

const loadCurrentUser = () => async dispatch => {
  const user = await authService.getCurrentUser();

  dispatch(setUser(user));
};

const getByUsername = username => async dispatch => {
  const user = await authService.getByUsername(username);

  if (user) {
    dispatch(setUsernameError(true));
  } else {
    dispatch(setUsernameError(false));
  }
};

const updateUser = payload => async dispatch => {
  const { id } = await authService.updateUser(payload);
  const user = await authService.getCurrentUser(id);

  dispatch(setUser(user));
};

const sharePostByUsername = payload => async () => {
  const user = await authService.sharePostByUsername(payload);
  return user;
};

const resetUserPassword = email => async () => {
  const user = await authService.resetUserPassword(email);
  return user;
};

const updateUserPassword = payload => async () => {
  const user = await authService.updateUserPassword(payload);
  return user;
};

export {
  setUser,
  setUsernameError,
  login,
  register,
  logout,
  loadCurrentUser,
  updateUser,
  getByUsername,
  sharePostByUsername,
  resetUserPassword,
  updateUserPassword
};
