import { createReducer } from '@reduxjs/toolkit';
import { setUser, setUsernameError } from './actions';

const initialState = {
  user: null,
  error: false
};

const reducer = createReducer(initialState, builder => {
  builder.addCase(setUser, (state, action) => {
    const { user } = action.payload;

    state.user = user;
  });

  builder.addCase(setUsernameError, (state, action) => {
    const { error } = action.payload;

    state.error = error;
  });
});

export { reducer };
