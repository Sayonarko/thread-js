import { createAction } from '@reduxjs/toolkit';
import {
  comment as commentService,
  post as postService
} from 'src/services/services';

const ActionType = {
  ADD_POST: 'thread/add-post',
  LOAD_MORE_POSTS: 'thread/load-more-posts',
  SET_ALL_POSTS: 'thread/set-all-posts',
  SET_EXPANDED_POST: 'thread/set-expanded-post',
  SET_DELETED_POST: 'thread/delete-post',
  SET_SHOW_LIKED_USERS: 'thread/liked-users'

};

const setPosts = createAction(ActionType.SET_ALL_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addMorePosts = createAction(ActionType.LOAD_MORE_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addPost = createAction(ActionType.ADD_POST, post => ({
  payload: {
    post
  }
}));

const setDeletedPost = createAction(ActionType.SET_DELETED_POST, id => ({
  payload: {
    id
  }
}));

const setExpandedPost = createAction(ActionType.SET_EXPANDED_POST, post => ({
  payload: {
    post
  }
}));

const setShowLikedUsers = createAction(ActionType.SET_SHOW_LIKED_USERS, users => ({
  payload: {
    users
  }
}));

const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPosts(posts));
};

const loadMorePosts = filter => async (dispatch, getRootState) => {
  const {
    posts: { posts }
  } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts.filter(
    post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
  );
  dispatch(addMorePosts(filteredPosts));
};

const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPost(post));
};

const createPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPost(newPost));
};

const updatePost = post => async dispatch => {
  const { id } = await postService.updatePost(post);
  const newPost = await postService.getPost(id);
  dispatch(setDeletedPost(id));
  dispatch(addPost(newPost));
};

const deletePost = postId => async dispatch => {
  const id = await postService.deletePost(postId);
  dispatch(setDeletedPost(id));
  dispatch(setExpandedPost(undefined));
};

const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPost(post));
};

const likePost = postId => async (dispatch, getRootState) => {
  await postService.likePost(postId);
  const users = await postService.getPostLikedUser(postId);
  const { likeCount, dislikeCount } = await postService.getPost(postId);
  const mapLikes = post => ({
    ...post,
    likeCount,
    dislikeCount
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPosts(updated));
  dispatch(setShowLikedUsers(users));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapLikes(expandedPost)));
  }
};

const dislikePost = postId => async (dispatch, getRootState) => {
  await postService.dislikePost(postId);
  const { likeCount, dislikeCount } = await postService.getPost(postId);
  const mapDislikes = post => ({
    ...post,
    likeCount,
    dislikeCount
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapDislikes(expandedPost)));
  }
};

const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: (Number(post.commentCount) + 1).toString(),
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

const likeComment = commentId => async (dispatch, getRootState) => {
  await commentService.likeComment(commentId);
  const users = await commentService.getCommentLikedUser(commentId);
  const { postId, dislikeCount, likeCount } = await commentService.getComment(commentId);
  const mapLikes = comment => ({
    ...comment,
    likeCount,
    dislikeCount
  });

  const mapComments = post => ({
    ...post,
    comments: post.comments
      ? post.comments.map(comment => (comment.id !== commentId ? comment : mapLikes(comment)))
      : []
  });
  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapComments(post)));
  dispatch(setPosts(updated));
  dispatch(setShowLikedUsers(users));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

const dislikeComment = commentId => async (dispatch, getRootState) => {
  await commentService.dislikeComment(commentId);
  const { postId, dislikeCount, likeCount } = await commentService.getComment(commentId);
  const mapLikes = comment => ({
    ...comment,
    dislikeCount,
    likeCount
  });
  const mapComments = post => ({
    ...post,
    comments: post.comments
      ? post.comments.map(comment => (comment.id !== commentId ? comment : mapLikes(comment)))
      : []
  });
  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapComments(post)));
  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

const deleteComment = commentId => async (dispatch, getRootState) => {
  const { id, postId } = await commentService.getComment(commentId);
  const response = await commentService.deleteComment(commentId);

  if (response) {
    const mapComments = post => ({
      ...post,
      comments: post.comments?.filter(comment => comment.id !== id),
      commentCount: (Number(post.commentCount) - 1).toString()
    });
    const {
      posts: { posts, expandedPost }
    } = getRootState();
    const updated = posts.map(post => (post.id !== postId ? post : mapComments(post)));
    dispatch(setPosts(updated));

    if (expandedPost && expandedPost.id === postId) {
      dispatch(setExpandedPost(mapComments(expandedPost)));
    }
  }
};

const updateComment = payload => async (dispatch, getRootState) => {
  const { id, postId, body } = await commentService.updateComment(payload);

  const mapComments = post => ({
    ...post,
    comments: post.comments?.map(comment => (comment.id !== id ? comment : { ...comment, body }))
  });
  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapComments(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

const getPostLikedUsers = postId => async dispatch => {
  const users = await postService.getPostLikedUser(postId);
  dispatch(setShowLikedUsers(users));
};

const getCommentLikedUsers = commentId => async dispatch => {
  const users = await commentService.getCommentLikedUser(commentId);
  dispatch(setShowLikedUsers(users));
};

export {
  setPosts,
  addMorePosts,
  addPost,
  setExpandedPost,
  setDeletedPost,
  setShowLikedUsers,
  loadPosts,
  loadMorePosts,
  applyPost,
  createPost,
  toggleExpandedPost,
  likePost,
  dislikePost,
  addComment,
  deletePost,
  getPostLikedUsers,
  updatePost,
  dislikeComment,
  likeComment,
  deleteComment,
  getCommentLikedUsers,
  updateComment
};
