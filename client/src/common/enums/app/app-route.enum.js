const AppRoute = {
  ROOT: '/',
  ANY: '*',
  LOGIN: '/login',
  REGISTRATION: '/registration',
  FORGOT: '/forgot',
  RESET_$TOKEN: '/reset/:token',
  PROFILE: '/profile',
  SHARE_$POSTHASH: '/share/:postHash'
};

export { AppRoute };
