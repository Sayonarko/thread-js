import * as React from 'react';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { Image } from 'src/components/common/common';
import PropTypes from 'prop-types';
import styles from './styles.module.scss';

const LikePopup = ({ showLikedUsers }) => {
  const popupRef = React.useRef();

  React.useEffect(() => {
    popupRef.current.parentNode.parentNode.style.filter = 'none';
  }, []);

  return (
    <div ref={popupRef} className={styles.popup}>
      {showLikedUsers.length === 1
          && (
            <>
              <Image
                centered
                src={showLikedUsers[0]?.user?.image?.link ?? DEFAULT_USER_AVATAR}
                circular
                className={styles.popupAvatar}
              />
              <span className={styles.popupText}>
                {showLikedUsers[0].user.username}
                {' '}
                liked this
              </span>
            </>
          )}
      {showLikedUsers.length > 1
           && (
             <div>
               <div style={{ display: 'flex', alignItems: 'center' }}>
                 <Image
                   centered
                   src={showLikedUsers[0]?.user?.image?.link ?? DEFAULT_USER_AVATAR}
                   circular
                   className={styles.popupAvatar}
                 />
                 <span className={styles.popupText}>
                   {showLikedUsers[0].user.username}
                   {', '}
                 </span>
                 <Image
                   centered
                   src={showLikedUsers[1]?.user?.image?.link ?? DEFAULT_USER_AVATAR}
                   circular
                   className={styles.popupAvatar}
                 />
                 <span className={styles.popupText}>
                   {showLikedUsers[1].user.username}
                 </span>
               </div>
               {showLikedUsers.length > 2 && (
                 <span>
                   and
                   {' '}
                   {showLikedUsers.length - 2}
                   {' '}
                   {showLikedUsers.length < 4 ? 'user' : 'users'}
                   {' '}
                 </span>
               )}
               liked this
             </div>
           )}
      {!showLikedUsers.length && <span>Nobody liked :(</span>}
    </div>
  );
};

LikePopup.defaultProps = {
  showLikedUsers: []
};

LikePopup.propTypes = {
  showLikedUsers: PropTypes.oneOfType([PropTypes.array])
};

export default LikePopup;
