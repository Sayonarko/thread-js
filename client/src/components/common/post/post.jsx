import * as React from 'react';
import PropTypes from 'prop-types';
import { getFromNowTime } from 'src/helpers/helpers';
import { IconName } from 'src/common/enums/enums';
import { postType } from 'src/common/prop-types/prop-types';
import { Icon, Card, Image, Label, Popup } from 'src/components/common/common';
import LikePopup from '../like-popup/like-popup';
import AddPost from '../../thread/components/add-post/add-post';
import styles from './styles.module.scss';

const Post = ({
  post,
  onPostLike,
  onPostDislike,
  onExpandedPostToggle,
  sharePost,
  onDeletePost,
  userId,
  getPostLikedUsers,
  showLikedUsers,
  uploadImage,
  postUpdate
}) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;
  const date = getFromNowTime(createdAt);

  const [openPostUpdate, setOpenPostUpdate] = React.useState(false);

  const handlePostLike = () => onPostLike(id);
  const handlePostDislike = () => onPostDislike(id);
  const handleExpandedPostToggle = () => onExpandedPostToggle(id);
  const handleDeletePost = () => onDeletePost(id);
  const handlePosltLikedUsers = () => getPostLikedUsers(id);
  const handlePostUpdate = data => {
    postUpdate({ id, data });
    setOpenPostUpdate(false);
  };
  return (
    openPostUpdate
      ? (
        <AddPost
          onPostAdd={handlePostUpdate}
          uploadImage={uploadImage}
          post={post}
        />
      )
      : (
        <Card style={{ width: '100%' }}>
          {image && <Image src={image.link} wrapped ui={false} />}
          <Card.Content>
            <Card.Meta>
              <span className="date">
                posted by
                {' '}
                {user.username}
                {' - '}
                {date}
              </span>
            </Card.Meta>
            <Card.Description>{body}</Card.Description>
          </Card.Content>
          <Card.Content extra>
            <Popup
              trigger={(
                <Label
                  basic
                  size="small"
                  as="a"
                  className={styles.toolbarBtn}
                  onClick={handlePostLike}
                  onMouseEnter={handlePosltLikedUsers}
                >
                  <Icon name={IconName.THUMBS_UP} />
                  {likeCount}
                </Label>
              )}
              content={<LikePopup showLikedUsers={showLikedUsers} />}
              on="hover"
              mouseEnterDelay={500}
            />
            <Label
              basic
              size="small"
              as="a"
              className={styles.toolbarBtn}
              onClick={handlePostDislike}
            >
              <Icon name={IconName.THUMBS_DOWN} />
              {dislikeCount}
            </Label>
            <Label
              basic
              size="small"
              as="a"
              className={styles.toolbarBtn}
              onClick={handleExpandedPostToggle}
            >
              <Icon name={IconName.COMMENT} />
              {commentCount}
            </Label>
            <Label
              basic
              size="small"
              as="a"
              className={styles.toolbarBtn}
              onClick={() => sharePost(id)}
            >
              <Icon name={IconName.SHARE_ALTERNATE} />
            </Label>
            {post.userId === userId && (
              <Label
                basic
                size="small"
                as="a"
                className={styles.toolbarBtn}
                onClick={() => setOpenPostUpdate(true)}
              >
                <Icon name={IconName.PENCIL} />
              </Label>
            )}
            {post.userId === userId && (
              <Label
                basic
                size="small"
                as="a"
                style={{ marginLeft: 'auto' }}
                className={styles.toolbarBtn}
                onClick={handleDeletePost}
              >
                <Icon name={IconName.TRASH} />
              </Label>
            )}
          </Card.Content>
        </Card>
      )
  );
};

Post.defaultProps = {
  showLikedUsers: []
};

Post.propTypes = {
  post: postType.isRequired,
  onPostLike: PropTypes.func.isRequired,
  onPostDislike: PropTypes.func.isRequired,
  onExpandedPostToggle: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired,
  onDeletePost: PropTypes.func.isRequired,
  getPostLikedUsers: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  postUpdate: PropTypes.func.isRequired,
  showLikedUsers: PropTypes.oneOfType([PropTypes.array])
};

export default Post;
