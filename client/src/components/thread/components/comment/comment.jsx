import * as React from 'react';
import { getFromNowTime } from 'src/helpers/helpers';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { Comment as CommentUI, Popup, Label, Icon } from 'src/components/common/common';
import { commentType } from 'src/common/prop-types/prop-types';
import { IconName } from 'src/common/enums/enums';
import { useSelector, useDispatch } from 'react-redux';
import { PropTypes } from 'prop-types';
import { threadActionCreator } from 'src/store/actions';
import LikePopup from '../../../common/like-popup/like-popup';
import styles from './styles.module.scss';
import AddComment from '../add-comment/add-comment';

const Comment = ({
  comment,
  handleCommentLike,
  handleCommentDislike,
  handleCommentDelete
}) => {
  const { user, createdAt, body, id, likeCount, dislikeCount } = comment;
  const { userId, showLikedUsers } = useSelector(state => ({
    userId: state.profile.user.id,
    showLikedUsers: state.posts.showLikedUsers || []
  }));
  const dispatch = useDispatch();

  const [openCommentUpdate, setOpenCommentUpdate] = React.useState(false);
  const handleCommentLikedUsers = React.useCallback(commentId => (
    dispatch(threadActionCreator.getCommentLikedUsers(commentId))
  ), [dispatch]);

  const handleCommentUpdate = React.useCallback(payload => (
    dispatch(threadActionCreator.updateComment(payload))
  ), [dispatch]);

  const commentLike = () => handleCommentLike(id);
  const commentDislike = () => handleCommentDislike(id);
  const commentDelete = () => handleCommentDelete(id);
  const commentLikedUsers = () => handleCommentLikedUsers(id);
  const commentUpdate = data => {
    handleCommentUpdate({ id, data });
    setOpenCommentUpdate(false);
  };

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={user.image?.link ?? DEFAULT_USER_AVATAR} />
      <CommentUI.Content>
        <CommentUI.Author as="a">{comment.user.username}</CommentUI.Author>
        <CommentUI.Metadata>{getFromNowTime(createdAt)}</CommentUI.Metadata>
        <CommentUI.Metadata style={{ display: 'block', marginLeft: 0 }}>{comment.user.status}</CommentUI.Metadata>
        {openCommentUpdate
          ? (
            <AddComment
              comment={comment}
              postId={comment.postId}
              onCommentAdd={commentUpdate}
            />
          )
          : <CommentUI.Text>{body}</CommentUI.Text>}
        {openCommentUpdate || (
          <CommentUI.Actions>
            <Popup
              className="popup"
              trigger={(
                <Label
                  basic
                  size="small"
                  as="a"
                  className={styles.toolbarBtn}
                  onClick={commentLike}
                  onMouseEnter={commentLikedUsers}
                >
                  <Icon name={IconName.THUMBS_UP} />
                  {likeCount}
                </Label>
              )}
              content={<LikePopup showLikedUsers={showLikedUsers} />}
              on="hover"
              mouseEnterDelay={500}
            />
            <Label
              basic
              size="small"
              as="a"
              className={styles.toolbarBtn}
              onClick={commentDislike}
            >
              <Icon name={IconName.THUMBS_DOWN} />
              {dislikeCount}
            </Label>
            {comment.userId === userId && (
              <Label
                basic
                size="small"
                as="a"
                className={styles.toolbarBtn}
                onClick={() => setOpenCommentUpdate(true)}
              >
                <Icon name={IconName.PENCIL} />
              </Label>
            )}
            {comment.userId === userId && (
              <Label
                basic
                size="small"
                as="a"
                style={{ marginLeft: 'auto' }}
                className={styles.toolbarBtn}
                onClick={commentDelete}
              >
                <Icon name={IconName.TRASH} />
              </Label>
            )}
          </CommentUI.Actions>
        )}

      </CommentUI.Content>
    </CommentUI>
  );
};
Comment.propTypes = {
  comment: commentType.isRequired,
  handleCommentLike: PropTypes.func.isRequired,
  handleCommentDislike: PropTypes.func.isRequired,
  handleCommentDelete: PropTypes.func.isRequired
};

export default Comment;
