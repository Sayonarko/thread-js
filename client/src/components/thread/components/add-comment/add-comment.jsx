import * as React from 'react';
import PropTypes from 'prop-types';
import { ButtonType } from 'src/common/enums/enums';
import { Button, Form } from 'src/components/common/common';
import { commentType } from '../../../../common/prop-types/comment';

const AddComment = ({ postId, onCommentAdd, comment }) => {
  const [body, setBody] = React.useState(comment?.body || '');

  const handleAddComment = async () => {
    if (!body) {
      return;
    }
    if (comment) {
      await onCommentAdd({ body });
    } else {
      await onCommentAdd({ postId, body });
      setBody('');
    }
  };

  return (
    <Form reply onSubmit={handleAddComment}>
      <Form.TextArea
        value={body}
        placeholder="Type a comment..."
        onChange={ev => setBody(ev.target.value)}
      />
      <Button type={ButtonType.SUBMIT} isPrimary>
        {comment ? 'Update comment' : 'Post comment'}
      </Button>
    </Form>
  );
};

AddComment.propTypes = {
  onCommentAdd: PropTypes.func.isRequired,
  postId: PropTypes.string.isRequired,
  comment: commentType
};

AddComment.defaultProps = {
  comment: undefined
};

export default AddComment;
