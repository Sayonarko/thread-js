import * as React from 'react';
import PropTypes from 'prop-types';
import { IconName, IconColor } from 'src/common/enums/enums';
import { Icon, Modal, Input } from 'src/components/common/common';
import { useSelector, useDispatch } from 'react-redux';
import { profileActionCreator } from 'src/store/actions';

import styles from './styles.module.scss';

const SharedPostLink = ({ postId, close }) => {
  const { error } = useSelector(state => ({
    error: state.profile.error
  }));
  const [isCopied, setIsCopied] = React.useState(false);
  const [emailSend, setEmailSend] = React.useState(false);
  const [username, setUsername] = React.useState('');
  const dispatch = useDispatch();
  let input = React.useRef();

  const copyToClipboard = ({ target }) => {
    input.select();
    document.execCommand('copy');
    target.focus();
    setIsCopied(true);
  };

  const handleSendMail = React.useCallback(payload => (
    dispatch(profileActionCreator.sharePostByUsername(payload))
      .then(user => {
        if (user) {
          setEmailSend(true);
          setUsername('');
          setTimeout(() => setEmailSend(false), 3000);
        }
      })
  ), [dispatch]);

  const sendMail = () => handleSendMail({ username, postId });

  React.useEffect(() => {
    if (username) {
      dispatch(profileActionCreator.getByUsername({ username }));
    }
  }, [dispatch, username]);

  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Share Post</span>
        {isCopied && (
          <span>
            <Icon name={IconName.COPY} color={IconColor.GREEN} />
            Copied
          </span>
        )}
        {emailSend && (
          <span>
            <Icon name="at" color={IconColor.GREEN} />
            Post sent
          </span>
        )}
      </Modal.Header>
      <Modal.Content>
        <div className={styles.shareUser}>
          <h3>Share post to another user:</h3>
          <Input
            icon="user"
            iconPosition="left"
            placeholder="Username"
            type="text"
            value={username}
            onChange={e => setUsername(e.target.value)}
            error={Boolean(username && !error)}
            action={{
              color: username && !error ? 'red' : 'teal',
              labelPosition: 'right',
              icon: IconName.AT,
              content: 'Send',
              onClick: sendMail,
              disabled: !username || (username && !error)
            }}
          />
          {username && !error && <span className={styles.notFound}>User not found</span>}
        </div>
        <Input
          fluid
          action={{
            color: 'teal',
            labelPosition: 'right',
            icon: 'copy',
            content: 'Copy',
            onClick: copyToClipboard
          }}
          value={`${window.location.origin}/share/${postId}`}
          ref={ref => {
            input = ref;
          }}
        />
      </Modal.Content>
    </Modal>
  );
};

SharedPostLink.propTypes = {
  postId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired
};

export default SharedPostLink;
