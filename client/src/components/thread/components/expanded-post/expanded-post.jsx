import * as React from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { threadActionCreator } from 'src/store/actions';
import { Spinner, Post, Modal, Comment as CommentUI } from 'src/components/common/common';
import AddComment from '../add-comment/add-comment';
import Comment from '../comment/comment';
import { getSortedComments } from './helpers/helpers';

const ExpandedPost = ({
  sharePost,
  onDeletePost,
  getPostLikedUsers,
  showLikedUsers,
  onPostAdd,
  postUpdate,
  uploadImage
}) => {
  const dispatch = useDispatch();
  const { post, userId } = useSelector(state => ({
    post: state.posts.expandedPost,
    userId: state.profile.user.id

  }));

  const handlePostLike = React.useCallback(id => (
    dispatch(threadActionCreator.likePost(id))
  ), [dispatch]);

  const handlePostDislike = React.useCallback(id => (
    dispatch(threadActionCreator.dislikePost(id))
  ), [dispatch]);

  const handleCommentAdd = React.useCallback(commentPayload => (
    dispatch(threadActionCreator.addComment(commentPayload))
  ), [dispatch]);

  const handleCommentLike = React.useCallback(commentId => (
    dispatch(threadActionCreator.likeComment(commentId))
  ), [dispatch]);

  const handleCommentDislike = React.useCallback(commentId => (
    dispatch(threadActionCreator.dislikeComment(commentId))
  ), [dispatch]);

  const handleCommentDelete = React.useCallback(commentId => (
    dispatch(threadActionCreator.deleteComment(commentId))
  ), [dispatch]);

  const handleExpandedPostToggle = React.useCallback(id => (
    dispatch(threadActionCreator.toggleExpandedPost(id))
  ), [dispatch]);

  const handleExpandedPostClose = () => handleExpandedPostToggle();

  const sortedComments = getSortedComments(post.comments ?? []);

  return (
    <Modal
      dimmer="blurring"
      centered={false}
      open
      onClose={handleExpandedPostClose}
    >
      {post ? (
        <Modal.Content>
          <Post
            userId={userId}
            post={post}
            onPostLike={handlePostLike}
            onPostDislike={handlePostDislike}
            onExpandedPostToggle={handleExpandedPostToggle}
            onDeletePost={onDeletePost}
            sharePost={sharePost}
            key={post.id}
            getPostLikedUsers={getPostLikedUsers}
            showLikedUsers={showLikedUsers}
            postUpdate={postUpdate}
            onPostAdd={onPostAdd}
            uploadImage={uploadImage}
          />
          <CommentUI.Group style={{ maxWidth: '100%' }}>
            <h3>Comments</h3>
            {sortedComments.map(comment => (
              <Comment
                key={comment.id}
                comment={comment}
                handleCommentLike={handleCommentLike}
                handleCommentDislike={handleCommentDislike}
                handleCommentDelete={handleCommentDelete}
              />
            ))}
            <AddComment postId={post.id} onCommentAdd={handleCommentAdd} />
          </CommentUI.Group>
        </Modal.Content>
      ) : (
        <Spinner />
      )}
    </Modal>
  );
};

ExpandedPost.defaultProps = {
  showLikedUsers: []
};

ExpandedPost.propTypes = {
  sharePost: PropTypes.func.isRequired,
  onDeletePost: PropTypes.func.isRequired,
  getPostLikedUsers: PropTypes.func.isRequired,
  showLikedUsers: PropTypes.oneOfType([PropTypes.array]),
  onPostAdd: PropTypes.func.isRequired,
  postUpdate: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired
};

export default ExpandedPost;
