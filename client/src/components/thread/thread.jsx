import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroller';
import { threadActionCreator } from 'src/store/actions';
import { image as imageService } from 'src/services/services';
import { Post, Spinner, Checkbox } from 'src/components/common/common';
import { ExpandedPost, SharedPostLink, AddPost } from './components/components';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  from: 0,
  count: 10,
  isLike: undefined,
  hideOwnPosts: undefined
};
const Thread = () => {
  const { posts, hasMorePosts, expandedPost, userId, showLikedUsers } = useSelector(state => ({
    posts: state.posts.posts,
    hasMorePosts: state.posts.hasMorePosts,
    expandedPost: state.posts.expandedPost,
    showLikedUsers: state.posts.showLikedUsers,
    userId: state.profile.user.id
  }));

  const [sharedPostId, setSharedPostId] = React.useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = React.useState(false);
  const [showLikedPosts, setShowLikedPosts] = React.useState(false);
  const [hideOwnPosts, setHideOwnPosts] = React.useState(false);

  const dispatch = useDispatch();

  const handlePostLike = React.useCallback(id => (
    dispatch(threadActionCreator.likePost(id))
  ), [dispatch]);

  const handlePostDislike = React.useCallback(id => (
    dispatch(threadActionCreator.dislikePost(id))
  ), [dispatch]);

  const handleExpandedPostToggle = React.useCallback(id => (
    dispatch(threadActionCreator.toggleExpandedPost(id))
  ), [dispatch]);

  const handlePostAdd = React.useCallback(postPayload => (
    dispatch(threadActionCreator.createPost(postPayload))
  ), [dispatch]);

  const handlePostDelete = React.useCallback(id => (
    dispatch(threadActionCreator.deletePost(id))
  ), [dispatch]);

  const handlePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadPosts(filtersPayload));
  };

  const handleMorePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadMorePosts(filtersPayload));
  };

  const postUpdate = React.useCallback(payload => (
    dispatch(threadActionCreator.updatePost(payload))
  ), [dispatch]);

  const getPostLikedUsers = postId => {
    dispatch(threadActionCreator.getPostLikedUsers(postId));
  };

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    postsFilter.userId = showOwnPosts ? undefined : userId;
    postsFilter.isLike = undefined;
    postsFilter.from = 0;
    handlePostsLoad(postsFilter);
    setShowLikedPosts(false);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowLikedPosts = () => {
    setShowLikedPosts(!showLikedPosts);
    postsFilter.isLike = showLikedPosts ? undefined : true;
    postsFilter.userId = showLikedPosts ? undefined : userId;
    postsFilter.hideOwnPosts = !hideOwnPosts ? undefined : true;
    postsFilter.from = 0;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count;
  };

  const togglehideOwnPosts = () => {
    setHideOwnPosts(!hideOwnPosts);
    if (showOwnPosts) setShowOwnPosts(!showOwnPosts);
    postsFilter.hideOwnPosts = hideOwnPosts ? undefined : true;
    if (!showLikedPosts) postsFilter.userId = hideOwnPosts ? undefined : userId;
    postsFilter.from = 0;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count;
  };

  const getMorePosts = () => {
    handleMorePostsLoad(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => setSharedPostId(id);

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost onPostAdd={handlePostAdd} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          disabled={hideOwnPosts}
          toggle
          label="Show only my posts"
          checked={showOwnPosts}
          onChange={toggleShowOwnPosts}
        />
        {'   '}
        <Checkbox
          toggle
          label="hide my posts"
          checked={hideOwnPosts}
          onChange={togglehideOwnPosts}
        />
        <Checkbox
          toggle
          label="Liked by me"
          checked={showLikedPosts}
          onChange={toggleShowLikedPosts}
        />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Spinner key="0" />}
      >
        {posts.map(post => (
          <Post
            userId={userId}
            post={post}
            onPostLike={handlePostLike}
            onPostDislike={handlePostDislike}
            onExpandedPostToggle={handleExpandedPostToggle}
            onDeletePost={handlePostDelete}
            sharePost={sharePost}
            key={post.id}
            getPostLikedUsers={getPostLikedUsers}
            showLikedUsers={showLikedUsers}
            postUpdate={postUpdate}
            uploadImage={uploadImage}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && (
        <ExpandedPost
          sharePost={sharePost}
          uploadImage={uploadImage}
          onPostAdd={handlePostAdd}
          postUpdate={postUpdate}
          showLikedUsers={showLikedUsers}
          getPostLikedUsers={getPostLikedUsers}
          onDeletePost={handlePostDelete}
        />
      )}
      {sharedPostId && (
        <SharedPostLink
          postId={sharedPostId}
          close={() => setSharedPostId(undefined)}
        />
      )}
    </div>
  );
};

export default Thread;
