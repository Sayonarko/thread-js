import * as React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Button, Grid, Image, Input } from 'src/components/common/common';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { image as imageService } from 'src/services/services';
import { profileActionCreator } from 'src/store/actions';
import { IconName } from '../../common/enums/enums';
import CropAvatar from './components/crop-avatar/crop-avatar';

const Profile = () => {
  const { user, error } = useSelector(state => ({
    user: state.profile.user,
    error: state.profile.error
  }));
  const [isEdit, setIsEdit] = React.useState(false);
  const [username, setUsername] = React.useState(user.username);
  const [image, setImage] = React.useState(user.image);
  const [status, setStatus] = React.useState(user.status);
  const [openCropAvatar, setOpenCropAvatar] = React.useState(undefined);
  const handleEditToggle = () => setIsEdit(!isEdit);
  const dispatch = useDispatch();

  React.useEffect(() => {
    if (username !== user.username) {
      dispatch(profileActionCreator.getByUsername({ username }));
    }
  }, [dispatch, user.username, username]);

  const handleUpdateProfile = () => {
    if (!username || error) {
      return;
    }
    dispatch(profileActionCreator.updateUser({ username, image, imageId: image?.id, status }));
    handleEditToggle();
  };

  const handleOnCrop = file => {
    imageService.uploadImage(file)
      .then(({ id, link }) => {
        setImage({ id, link });
        setOpenCropAvatar(null);
      })
      .catch(() => {
        // TODO: show error
      })
      .finally(() => {
        setOpenCropAvatar(null);
      });
  };

  const handleUploadFile = ({ target }) => {
    const [file] = target.files;

    if (file) {
      const imageUrl = URL.createObjectURL(file);
      setOpenCropAvatar(imageUrl);
    }
  };

  return (
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Column>
        {isEdit
          ? (
            <label
              style={{ cursor: 'pointer' }}
              htmlFor="avatar"
            >
              <Image
                centered
                src={image?.link || user.image?.link || DEFAULT_USER_AVATAR}
                size="medium"
                circular
              />
              <input
                type="file"
                id="avatar"
                accept="image/*"
                style={{ display: 'none' }}
                onChange={handleUploadFile}
              />
            </label>
          )
          : (
            <Image
              centered
              src={user.image?.link ?? DEFAULT_USER_AVATAR}
              size="medium"
              circular
            />
          )}
        <br />
        <Input
          icon="user"
          iconPosition="left"
          placeholder="Username"
          type="text"
          disabled={!isEdit}
          value={username}
          error={error}
          onChange={e => setUsername(e.target.value)}
        />
        {error
          ? <div style={{ color: '#9f3a38' }}>Username is already in use</div>
          : (
            <>
              <br />
              <br />
            </>
          )}
        <Input
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          disabled
          value={user.email}
        />
        <br />
        <br />
        <Input
          icon={IconName.PENCIL}
          iconPosition="left"
          placeholder="Write your thoughts..."
          type="text"
          disabled={!isEdit}
          value={status}
          onChange={e => setStatus(e.target.value)}
        />
        <br />
        <br />
        <Button
          onClick={isEdit ? handleUpdateProfile : handleEditToggle}
        >
          {isEdit ? 'Save profile' : 'Edit profile'}
        </Button>
      </Grid.Column>
      {openCropAvatar && (
        <CropAvatar
          file={openCropAvatar}
          onCrop={handleOnCrop}
          close={() => setOpenCropAvatar(undefined)}
        />
      )}
    </Grid>
  );
};

export default Profile;
