import * as React from 'react';
import PropTypes from 'prop-types';
import { Spinner, Modal, Button, Segment } from 'src/components/common/common';
import ReactCrop from 'react-image-crop';

import 'react-image-crop/dist/ReactCrop.css';

const CropAvatar = ({ file, onCrop, close }) => {
  const [crop, setCrop] = React.useState({ unit: 'px', x: 0, y: 0, width: 100, height: 100, aspect: 1 / 1 });
  const [completedCrop, setCompletedCrop] = React.useState(null);
  const [urlFile, setUrlFile] = React.useState(null);

  React.useEffect(() => {
    if (!completedCrop) {
      return;
    }

    const image = new Image();
    image.src = file;
    const canvas = document.createElement('canvas');

    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    const ctx = canvas.getContext('2d');
    const pixelRatio = window.devicePixelRatio;

    canvas.width = completedCrop.width * pixelRatio;
    canvas.height = completedCrop.height * pixelRatio;

    ctx.setTransform(pixelRatio, 0, 0, pixelRatio, 0, 0);
    ctx.imageSmoothingQuality = 'high';

    ctx.drawImage(
      image,
      completedCrop.x * scaleX,
      completedCrop.y * scaleY,
      completedCrop.width * scaleX,
      completedCrop.height * scaleY,
      0,
      0,
      completedCrop.width,
      completedCrop.height
    );

    canvas.toBlob(
      blob => {
        const cropFile = new File([blob], 'avatar.jpg', { type: 'image/jpeg' });
        setUrlFile(cropFile);
      }
    );
  }, [completedCrop, file]);

  return (
    <Modal
      dimmer="blurring"
      centered={false}
      open
      onClose={close}
      style={{ overflow: 'hidden' }}
    >
      <Modal.Header>
        <span>Update avatar</span>
      </Modal.Header>
      {file ? (
        <Modal.Content>
          <ReactCrop
            maxWidth="200"
            maxHeight="200"
            minWidth="50"
            minHeight="50"
            imageAlt="avatar"
            imageStyle={{ maxHeight: '500px' }}
            src={file}
            crop={crop}
            onChange={c => setCrop(c)}
            onComplete={c => setCompletedCrop(c)}
          />
          <Segment align="center">
            <Button onClick={() => onCrop(urlFile)}>Update</Button>
          </Segment>
        </Modal.Content>
      ) : (
        <Spinner />
      )}
    </Modal>
  );
};

CropAvatar.propTypes = {
  file: PropTypes.string.isRequired,
  onCrop: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired
};

export default CropAvatar;
