import * as React from 'react';
import PropTypes from 'prop-types';
import validator from 'validator';
import { useHistory } from 'react-router-dom';
import {
  ButtonType,
  ButtonSize,
  ButtonColor
} from 'src/common/enums/enums';
import {
  Button,
  Form,
  Segment
} from 'src/components/common/common';
import styles from './styles.module.scss';

const ForgotForm = ({ onResetPassword }) => {
  const [email, setEmail] = React.useState('');
  const [isLoading, setIsLoading] = React.useState(false);
  const [isEmailValid, setIsEmailValid] = React.useState(true);
  const history = useHistory();

  const emailChanged = data => {
    setEmail(data);
    setIsEmailValid(true);
  };

  const handleClick = () => {
    if (!isEmailValid || isLoading) {
      return;
    }
    setIsLoading(true);

    onResetPassword({ email })
      .then(user => user && history.push('/'))
      .catch(() => {
        setIsEmailValid(false);
        setIsLoading(false);
      });
  };

  return (
    <>
      <h2 className={styles.title}>Reset password</h2>
      <div className={styles.text}>We will send a link to reset your password to your email</div>
      <Form name="loginForm" size="large" onSubmit={handleClick}>
        <Segment>
          <Form.Input
            fluid
            icon="at"
            iconPosition="left"
            placeholder="Email"
            type="email"
            error={!isEmailValid}
            onChange={ev => emailChanged(ev.target.value)}
            onBlur={() => setIsEmailValid(validator.isEmail(email))}
          />
          <Button
            type={ButtonType.SUBMIT}
            color={ButtonColor.TEAL}
            size={ButtonSize.LARGE}
            isLoading={isLoading}
            isFluid
            isPrimary
          >
            Send
          </Button>
        </Segment>
      </Form>
    </>
  );
};

ForgotForm.propTypes = {
  onResetPassword: PropTypes.func.isRequired
};

export default ForgotForm;
