import * as React from 'react';
import PropTypes from 'prop-types';
import { useHistory, useLocation } from 'react-router-dom';
import {
  ButtonType,
  ButtonSize,
  ButtonColor,
  AppRoute } from 'src/common/enums/enums';
import {
  Button,
  Form,
  Segment,
  NavLink
} from 'src/components/common/common';

import styles from './styles.module.scss';

const ResetForm = ({ onUpdatePassword }) => {
  const [password, setPassword] = React.useState('');
  const [confirmPassword, setConfirmPassword] = React.useState('');
  const [isLoading, setIsLoading] = React.useState(false);
  const [isPasswordValid, setIsPasswordValid] = React.useState(true);
  const [error, setError] = React.useState(false);
  const history = useHistory();
  const location = useLocation();
  const token = location.pathname.replace(/^\/\w+\//, '');

  const handleClick = () => {
    setIsLoading(true);
    if (isPasswordValid) {
      onUpdatePassword({ token, password })
        .then(user => user && history.push('/'))
        .catch(() => {
        // TODO: show error
          setError(true);
          setIsLoading(false);
        });
    }
  };

  React.useEffect(() => {
    if (confirmPassword && password !== confirmPassword) {
      setIsPasswordValid(false);
    } else {
      setIsPasswordValid(true);
    }
  }, [password, confirmPassword]);

  return (
    <>
      {error
        ? <h2 className={styles.titleError}>link expired!</h2>
        : <h2 className={styles.title}>New password</h2>}
      <Form name="loginForm" size="large" onSubmit={handleClick}>
        <Segment>
          <Form.Input
            fluid
            icon="lock"
            iconPosition="left"
            placeholder="Password"
            type="password"
            value={password}
            onChange={ev => setPassword(ev.target.value)}
          />
          <Form.Input
            fluid
            icon="lock"
            iconPosition="left"
            placeholder="Confirm password"
            type="password"
            error={!isPasswordValid}
            value={confirmPassword}
            onChange={ev => setConfirmPassword(ev.target.value)}
          />
          <Button
            type={ButtonType.SUBMIT}
            color={ButtonColor.TEAL}
            size={ButtonSize.LARGE}
            isLoading={isLoading}
            isFluid
            isPrimary
          >
            Send
          </Button>
        </Segment>
        {error && <NavLink to={AppRoute.FORGOT}>Try again</NavLink>}
      </Form>
    </>
  );
};

ResetForm.propTypes = {
  onUpdatePassword: PropTypes.func.isRequired
};

export default ResetForm;
