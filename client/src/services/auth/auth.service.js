import { HttpMethod, ContentType } from 'src/common/enums/enums';

class Auth {
  constructor({ http }) {
    this._http = http;
  }

  login(payload) {
    return this._http.load('/api/auth/login', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify(payload)
    });
  }

  registration(payload) {
    return this._http.load('/api/auth/register', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify(payload)
    });
  }

  getCurrentUser() {
    return this._http.load('/api/auth/user', {
      method: HttpMethod.GET
    });
  }

  getByUsername(username) {
    return this._http.load('/api/auth/user', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(username)
    });
  }

  updateUser(payload) {
    return this._http.load('/api/auth/user', {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      hasAuth: true,
      payload: JSON.stringify(payload)
    });
  }

  sharePostByUsername(payload) {
    return this._http.load('/api/auth/share', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  resetUserPassword(email) {
    return this._http.load('/api/auth/reset', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify(email)
    });
  }

  updateUserPassword(payload) {
    return this._http.load('/api/auth/reset', {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify(payload)
    });
  }
}

export { Auth };
