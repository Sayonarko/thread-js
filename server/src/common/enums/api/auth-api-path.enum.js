const AuthApiPath = {
  LOGIN: '/login',
  REGISTER: '/register',
  USER: '/user',
  SHARE: '/share',
  RESET: '/reset',
};

export { AuthApiPath };
