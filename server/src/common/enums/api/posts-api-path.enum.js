const PostsApiPath = {
  ROOT: '/',
  $ID: '/:id',
  REACT: '/react',
  REACT$ID: '/react/:id',
};

export { PostsApiPath };
