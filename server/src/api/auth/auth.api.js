import { AuthApiPath } from '../../common/enums/enums';
import {
  authentication as authenticationMiddleware,
  registration as registrationMiddleware,
  jwt as jwtMiddleware
} from '../../middlewares/middlewares';
import { mailer } from '../../services/send-mail/send-mail.service';
import { message } from '../../services/send-mail/message';
import { encrypt, createToken } from '../../helpers/helpers';

const initAuth = (Router, services) => {
  const { auth: authService, user: userService } = services;
  const router = Router();

  // user added to the request (req.user) in a strategy, see passport config
  router
    .post(AuthApiPath.LOGIN, authenticationMiddleware, (req, res, next) => authService
      .login(req.user)
      .then(data => res.send(data))
      .catch(next))
    .post(AuthApiPath.REGISTER, registrationMiddleware, (req, res, next) => authService
      .register(req.user)
      .then(data => res.send(data))
      .catch(next))
    .get(AuthApiPath.USER, jwtMiddleware, (req, res, next) => userService
      .getUserById(req.user.id)
      .then(data => res.send(data))
      .catch(next))
    .post(AuthApiPath.USER, jwtMiddleware, (req, res, next) => userService
      .getByUsername(req.body.username)
      .then(data => res.json(data))
      .catch(next))
    .put(AuthApiPath.USER, jwtMiddleware, (req, res, next) => userService
      .updateUserById(req.user.id, req.body)
      .then(data => {
        req.io
          .to(req.user.id)
          .emit('profile', 'Your profile was updated!');
        return res.send(data)
      })
      .catch(next))
    .post(AuthApiPath.SHARE, (req, res, next) => userService
      .getByUsername(req.body.username)
      .then(user => {
        mailer(message.SHARE_POST(req, user));
        return res.send(user);
      })
      .catch(next))
    .post(AuthApiPath.RESET, (req, res, next) => userService
      .getByEmail(req.body.email)
      .then(user => {
        if (user) {
          const token = createToken({ id: user.updatedAt });
          userService.updateUserById(user.id, {
            resetPasswordToken: token,
            resetPasswordExpires: Date.now() + 900000
          })
            .then(user => mailer(message.RESET_PASSWORD(user, user.resetPasswordToken)))
          return res.send(user);
        } else {
          return res.status(404).send('Email not found!');
        }

      })
      .catch(next))
    .put(AuthApiPath.RESET, (req, res, next) => userService
      .getUserByResetPasswordToken(req.body.token)
      .then(user => {
        if (user) {
          encrypt(req.body.password)
            .then(password => userService.updateUserById(user.id, {
              password,
              resetPasswordToken: null,
              resetPasswordExpires: null
            }))
            .then(user => res.send(user))
        } else {
          return res.sendStatus(404)
        }
      })
      .catch(next))

  return router;
};

export { initAuth };
