import { PostsApiPath } from '../../common/enums/enums';
import { mailer } from '../../services/send-mail/send-mail.service';
import { message } from '../../services/send-mail/message';

const initPost = (Router, services) => {
  const { post: postService, user: userService } = services;
  const router = Router();

  router
    .get(PostsApiPath.ROOT, (req, res, next) => postService
      .getPosts(req.query)
      .then(posts => res.send(posts))
      .catch(next))
    .get(PostsApiPath.$ID, (req, res, next) => postService
      .getPostById(req.params.id)
      .then(post => res.send(post))
      .catch(next))
    .post(PostsApiPath.ROOT, (req, res, next) => postService
      .create(req.user.id, req.body)
      .then(post => {
        req.io.emit('new_post', post); // notify all users that a new post was created
        return res.send(post);
      })
      .catch(next))
    .put(PostsApiPath.ROOT, (req, res, next) => postService
      .updatePostById(req.body)
      .then(post => res.send(post))
      .catch(next))
    .put(PostsApiPath.REACT, (req, res, next) => postService
      .setReaction(req.user.id, req.body)
      .then(reaction => {
        if (reaction.post) {
          userService.getUserById(reaction.post.userId)
            .then(user => {
              if (reaction.post.userId !== req.user.id) {
                // notify a user if someone (not himself) liked his post
                req.io
                  .to(reaction.post.userId)
                  .emit('like', 'Your post was liked!');
                mailer(message.LIKE_POST(req, reaction, user));
              } else if (reaction.post && reaction.post.userId !== req.user.id && !reaction.isLike) {
                // notify a user if someone (not himself) disliked his post
                req.io
                  .to(reaction.post.userId)
                  .emit('dislike', 'Your post was disliked!');
              }
            })
        }
        return res.send(reaction);
      })
      .catch(next))
    .get(PostsApiPath.REACT$ID, (req, res, next) => postService
      .getPostLikedUser(req.params.id)
      .then(post => res.send(post))
      .catch(next))
    .delete(PostsApiPath.$ID, (req, res, next) => postService
      .deleteById(req.params.id)
      .then(post => {
        if (post) {
          req.io
            .to(req.user.id)
            .emit('delete_post', 'Youre post was deleted!');
        }
        return res.json(req.params.id)
      })
      .catch(next));

  return router;
};

export { initPost };
