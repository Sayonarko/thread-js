import { CommentsApiPath } from '../../common/enums/enums';

const initComment = (Router, services) => {
  const { comment: commentService } = services;
  const router = Router();

  router
    .get(CommentsApiPath.$ID, (req, res, next) => commentService
      .getCommentById(req.params.id)
      .then(comment => res.send(comment))
      .catch(next))
    .post(CommentsApiPath.ROOT, (req, res, next) => commentService
      .create(req.user.id, req.body)
      .then(comment => res.send(comment))
      .catch(next))
    .put(CommentsApiPath.ROOT, (req, res, next) => commentService
      .updateComment(req.body)
      .then(comment => res.send(comment))
      .catch(next))
    .delete(CommentsApiPath.$ID, (req, res, next) => commentService
      .deleteCommentById(req.params.id)
      .then(comment => res.json(comment))
      .catch(next))
    .put(CommentsApiPath.REACT, (req, res, next) => commentService
      .setReaction(req.user.id, req.body)
      .then(reaction => res.send(reaction))
      .catch(next))
    .post(CommentsApiPath.REACT, (req, res, next) => commentService
      .getCommentLikedUser(req.body.commentId)
      .then(post => res.send(post))
      .catch(next))

  return router;
};

export { initComment };
