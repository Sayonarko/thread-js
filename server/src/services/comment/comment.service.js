class Comment {
  constructor({ commentRepository, commentReactionRepository }) {
    this._commentRepository = commentRepository;
    this._commentReactionRepository = commentReactionRepository;

  }

  create(userId, comment) {
    return this._commentRepository.create({
      ...comment,
      userId
    });
  }

  getCommentById(id) {
    return this._commentRepository.getCommentById(id);
  }

  async setReaction(userId, { commentId, isLike = true }) {
    // define the callback for future use as a promise
    const updateOrDelete = react => (react.isLike === isLike
      ? this._commentReactionRepository.deleteById(react.id)
      : this._commentReactionRepository.updateById(react.id, { isLike }));

    const reaction = await this._commentReactionRepository.getCommentReaction(
      userId,
      commentId
    );
    const result = reaction
      ? await updateOrDelete(reaction)
      : await this._commentReactionRepository.create({ userId, commentId, isLike });
    // the result is an integer when an entity is deleted
    return Number.isInteger(result)
      ? {}
      : this._commentReactionRepository.getCommentReaction(userId, commentId);
  }

  getCommentLikedUser(commentId) {
    return this._commentReactionRepository.getCommentLikedUser(commentId);
  }

  deleteCommentById(id) {
    return this._commentRepository.deleteById(id);
  }
  updateComment(payload) {
    return this._commentRepository.updateById(payload);
  }
}

export { Comment };
