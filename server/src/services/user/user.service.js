class User {
  constructor({ userRepository }) {
    this._userRepository = userRepository;
  }

  async getUserById(id) {
    const user = await this._userRepository.getUserById(id);

    return user;
  }

  async updateUserById(id, data) {
    const user = await this._userRepository.updateUserById(id, data);

    return user;
  }

  async getByUsername(username) {
    const user = await this._userRepository.getByUsername(username);

    return user;
  }
  async getByEmail(email) {
    const user = await this._userRepository.getByEmail(email);

    return user;
  }

  async getUserByResetPasswordToken(token) {
    const user = await this._userRepository.getUserByResetPasswordToken(token);

    return user;
  }
}

export { User };
