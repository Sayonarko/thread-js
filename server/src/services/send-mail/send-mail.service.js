import nodemailer from 'nodemailer';
import { ENV } from '../../common/enums/enums';

const transporter = nodemailer.createTransport({
    host: 'smtp.ethereal.email',
    port: 587,
    secure: false,
    auth: {
        user: ENV.ETHEREAL_MAIL.USER,
        pass: ENV.ETHEREAL_MAIL.PASS
    }
});

const mailer = message => {
    transporter.sendMail(message, (err, info) => {
        if (err) return console.log(err);
        console.log('Email sent: ', info.envelope)
    });
}

export { mailer };