import { ENV } from '../../common/enums/enums';

const message = {
    LIKE_POST: function (req, reaction, user) {
        return ({
            to: user.email,
            from: `Thread Mailer <${ENV.ETHEREAL_MAIL.USER}>`,
            subject: 'Someone liked your post',
            html: `<br />
                  <h2>Hello.</h2>
                  <br />
                  <p>Your post was liked by <b>${req.user.username}.</b></p>
                  <br />
                  <p>Post: <a href="http://localhost:3000/share/${reaction.postId}" target="_blank">http://localhost:3000/share/${reaction.postId}</a></p>`
        });
    },
    SHARE_POST: function (req, user) {
        return ({
            to: user.email,
            from: `Thread Mailer <${ENV.ETHEREAL_MAIL.USER}>`,
            subject: 'Someone shared a post with you',
            html: `<br />
                  <h2>Hello.</h2>
                  <br />
                  <p><b>${req.user.username}</b> shared a post with you.</p>
                  <br />
                  <p>Have a look: <a href="http://localhost:3000/share/${req.body.postId}" target="_blank">http://localhost:3000/share/${req.body.postId}</a></p>`
        })
    },
    RESET_PASSWORD: function (user, token) {
        return ({
            to: user.email,
            from: `Thread Mailer <${ENV.ETHEREAL_MAIL.USER}>`,
            subject: 'Thread reset password',
            html: `<br />
                  <h2>Hello.</h2>
                  <br />
                  <p>Your password reset link: <a href="http://localhost:3000/reset/${token}" target="_blank">click to reset password</a></p>
                  <p>the link will be available within 15 minutes</p>`
        })
    }
}

export { message };