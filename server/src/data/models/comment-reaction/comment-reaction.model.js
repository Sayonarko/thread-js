import { DataTypes } from 'sequelize';

const init = orm => {
  const CommentReaction = orm.define(
    'commentReaction',
    {
      isLike: {
        allowNull: false,
        type: DataTypes.BOOLEAN,
        defaultValue: true
      },
      createdAt: DataTypes.DATE,
      updatedAt: DataTypes.DATE
    },
    {}
  );

  return CommentReaction;
};

export { init };