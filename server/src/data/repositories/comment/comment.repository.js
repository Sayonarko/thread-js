import { Abstract } from '../abstract/abstract.repository';
import { sequelize } from '../../db/connection';

const commentlikeCase = bool => `CASE WHEN "commentReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

class Comment extends Abstract {
  constructor({ commentModel, userModel, imageModel, commentReactionModel }) {
    super(commentModel);
    this._userModel = userModel;
    this._imageModel = imageModel;
    this._commentReactionModel = commentReactionModel;
  }

  getCommentById(id) {
    return this.model.findOne({
      group: ['comment.id', 'user.id', 'user->image.id', 'commentReactions.id'],
      where: { id },
      attributes: {
        include: [
          [sequelize.fn('SUM', sequelize.literal(commentlikeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(commentlikeCase(false))), 'dislikeCount']
        ]
      },
      include: [
        {
          model: this._userModel,
          attributes: ['id', 'username', 'status'],
          include: {
            model: this._imageModel,
            attributes: ['id', 'link']
          }
        },
        {
          model: this._commentReactionModel,
          attributes: []
        }
      ]
    });
  }

  async updateById({ id, data }) {
    const result = await this.model.update(data, {
      where: { id },
      returning: true,
      plain: true
    });

    return result[1];
  }
}

export { Comment };
