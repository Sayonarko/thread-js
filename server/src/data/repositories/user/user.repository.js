import { Abstract } from '../abstract/abstract.repository';
import { Op } from 'sequelize';

class User extends Abstract {
  constructor({ userModel, imageModel }) {
    super(userModel);
    this._imageModel = imageModel;
  }

  addUser(user) {
    return this.create(user);
  }

  getByEmail(email) {
    return this.model.findOne({ where: { email } });
  }

  getByUsername(username) {
    return this.model.findOne({ where: { username } });
  }

  getUserById(id) {
    return this.model.findOne({
      group: ['user.id', 'image.id'],
      attributes: ['id', 'createdAt', 'email', 'updatedAt', 'username', 'status'],
      where: { id },
      include: {
        model: this._imageModel,
        attributes: ['id', 'link']
      }
    });
  }

  getUserByResetPasswordToken(token) {
    return this.model.findOne({
      where: {
        resetPasswordToken: token,
        resetPasswordExpires: {
          [Op.gt]: Date.now()
        }
      }
    });

  }

  async updateUserById(id, data) {
    const result = await this.model.update(data, {
      where: { id },
      returning: true,
      plain: true
    });

    return result[1];
  }
}

export { User };
