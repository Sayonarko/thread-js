import { sequelize } from '../../db/connection';
import { Op } from "sequelize";
import { Abstract } from '../abstract/abstract.repository';

const likeCase = bool => `CASE WHEN "postReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;
const commentlikeCase = bool => `CASE WHEN "comments->commentReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;



class Post extends Abstract {
  constructor({
    postModel,
    commentModel,
    userModel,
    imageModel,
    postReactionModel,
    commentReactionModel

  }) {
    super(postModel);
    this._commentModel = commentModel;
    this._userModel = userModel;
    this._imageModel = imageModel;
    this._postReactionModel = postReactionModel;
    this._commentReactionModel = commentReactionModel;
  }

  async getPosts(filter) {
    const {
      from: offset,
      count: limit,
      userId,
      isLike,
      hideOwnPosts
    } = filter;

    const where = {};
    if (userId && !isLike) {
      Object.assign(where, { userId });
    }

    if (userId && !isLike && hideOwnPosts) {
      Object.assign(where, {
        userId: {
          [Op.ne]: userId
        }
      });
    }
    if (isLike) {
      const where = {};
      if (hideOwnPosts) {
        Object.assign(where, {
          userId: {
            [Op.ne]: userId
          }
        });
      }
      return this.model.findAll({
        where,
        attributes: {
          include: [
            [sequelize.literal(`
            (SELECT COUNT(*)
            FROM "comments" as "comment"
            WHERE "post"."id" = "comment"."postId"
            AND "comment"."deletedAt" IS NULL)`), 'commentCount'],
            [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
            [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
          ]
        },
        include: [{
          model: this._imageModel,
          attributes: ['id', 'link']
        }, {
          model: this._userModel,
          attributes: ['id', 'username'],
          include: {
            model: this._imageModel,
            attributes: ['id', 'link']
          }
        }, {
          model: this._postReactionModel,
          attributes: [],
          where: { userId, isLike },
          duplicating: false
        }],
        group: [
          'post.id',
          'image.id',
          'user.id',
          'user->image.id'
        ],
        order: [['createdAt', 'DESC']],
        offset,
        limit
      });
    }

    return this.model.findAll({
      where,
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId"
                        AND "comment"."deletedAt" IS NULL)`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
        ]
      },
      include: [{
        model: this._imageModel,
        attributes: ['id', 'link']
      }, {
        model: this._userModel,
        attributes: ['id', 'username'],
        include: {
          model: this._imageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: this._postReactionModel,
        attributes: [],
        duplicating: false
      }],
      group: [
        'post.id',
        'image.id',
        'user.id',
        'user->image.id'
      ],
      order: [['createdAt', 'DESC']],
      offset,
      limit
    });
  }

  getPostById(id) {
    return this.model.findOne({
      group: [
        'post.id',
        'comments.id',
        'comments->user.id',
        'comments->user->image.id',
        'comments->commentReactions.id',
        'user.id',
        'user->image.id',
        'image.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(`
          (SELECT COUNT(*)
          FROM "comments" as "comment"
          WHERE "post"."id" = "comment"."postId"
          AND "comment"."deletedAt" IS NULL)`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
        ]
      },
      include: [{
        model: this._commentModel,
        attributes: {
          include: [
            [sequelize.fn('SUM', sequelize.literal(commentlikeCase(true))), 'likeCount'],
            [sequelize.fn('SUM', sequelize.literal(commentlikeCase(false))), 'dislikeCount']
          ]
        },
        include: [
          {
            model: this._userModel,
            attributes: ['id', 'username', 'status'],
            include: {
              model: this._imageModel,
              attributes: ['id', 'link']
            }
          },
          {
            model: this._commentReactionModel,
            attributes: []
          }]
      }, {
        model: this._userModel,
        attributes: ['id', 'username'],
        include: {
          model: this._imageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: this._imageModel,
        attributes: ['id', 'link']
      }, {
        model: this._postReactionModel,
        attributes: []
      }]
    });
  }

  deleteById(id) {
    return this.model.destroy({
      where: { id }
    });
  }

  async updatePostById({ id, data }) {
    const result = await this.model.update(data, {
      where: { id },
      returning: true,
      plain: true
    });

    return result[1];
  }
}

export { Post };
