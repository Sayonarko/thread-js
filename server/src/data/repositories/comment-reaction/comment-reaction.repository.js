import { Abstract } from '../abstract/abstract.repository';

class CommentReaction extends Abstract {
  constructor({ commentReactionModel, commentModel, userModel, imageModel }) {
    super(commentReactionModel);
    this._commentModel = commentModel;
    this._userModel = userModel;
    this._imageModel = imageModel;
  }

  getCommentReaction(userId, commentId) {
    return this.model.findOne({
      group: ['commentReaction.id', 'comment.id'],
      where: { userId, commentId },
      include: [
        {
          model: this._commentModel,
          attributes: ['id', 'userId']
        }
      ]
    });
  }

  getCommentLikedUser(commentId) {
    return this.model.findAll({
      where: { commentId, isLike: true },
      include: [{
        model: this._userModel,
        attributes: ['username'],
        include: {
          model: this._imageModel,
          attributes: ['id', 'link']
        }
      }],
      group: [
        'commentReaction.id',
        'user.id',
        'user->image.id'
      ],
      order: [['createdAt', 'DESC']],
    });
  }
}

export { CommentReaction };
